﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ti.Me.Model;

namespace Ti.Me.Logic
{
    internal class TrackerController
    {
        internal Task<IEnumerable<Issue>> RecentIssues(string filter, int limit)
        {
            return Task.Run(() =>
            {
                var db = new Data.Database();
                if (filter == string.Empty)
                {
                    var data = db.Issues().Result;
                    return data
                        .OrderByDescending(n => n.LastTracked)
                        .ThenBy(n => n.Key)
                        .Take(limit);
                }
                else
                {
                    filter = filter.ToUpper();

                    var data = db.Issues(n => 
                        n.Key.ToUpper().Contains(filter) || 
                        (n.Summary != null && n.Summary.ToUpper().Contains(filter)))
                        .Result;

                    return data
                        .OrderByDescending(n => n.LastTracked)
                        .ThenBy(n => n.Key)
                        .Take(limit);
                }
            });
        }

        internal Task<Tracker> StartTracking(string key, int roundMinutesUp, Model.Tracker current = null)
        {
            return Task.Run(() =>
            {

                key = key.ToUpper();
                if (current != null)
                {
                    EndTracking(current, roundMinutesUp);
                    // stop here if the current tracker = new tracker
                    if (current.Key == key) return null;
                }

                var db = new Data.Database();
                var issue = db.GetIssue(key).Result;
                issue.LastTracked = DateTime.Now;
                db.UpsertIssue(issue);

                var tracker = new Model.Tracker()
                {
                    Key = key,
                    Start = DateTime.Now,
                    Minutes = 0,
                    SyncedWithJira = false
                };

                return tracker;
            });

        }

        internal Task EndTracking(Tracker tracker, int roundMinutesUp)
        {
            return Task.Run(() =>
            {
                tracker.End = DateTime.Now;
                tracker.Minutes = (int)Math.Round(tracker.End.Value.Subtract(tracker.Start).TotalMinutes);

                if (tracker.Minutes == 0) return;

                // round minutes
                if (roundMinutesUp > 1)
                {
                    tracker.Minutes = ((int)Math.Ceiling((double)tracker.Minutes / roundMinutesUp)) * roundMinutesUp;
                }

                var db = new Data.Database();
                db.UpsertTracker(tracker);

                var issueSearch = db.Issues(n => n.Key == tracker.Key).Result;
                if (issueSearch.Any())
                {
                    var issue = issueSearch.First();
                    issue.Minutes += tracker.Minutes;
                    db.UpsertIssue(issue);
                }
            });
        }

    }
}
