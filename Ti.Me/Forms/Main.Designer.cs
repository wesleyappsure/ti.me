﻿namespace Ti.Me.Forms
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BrightIdeasSoftware.OLVColumn olvColumn3;
            BrightIdeasSoftware.OLVColumn olvColumn4;
            MetroFramework.Controls.MetroLabel metroLabel1;
            MetroFramework.Controls.MetroLabel metroLabel2;
            MetroFramework.Controls.MetroButton HistoryApplyButton;
            MetroFramework.Controls.MetroLabel metroLabel3;
            BrightIdeasSoftware.OLVColumn olvColumn1;
            MetroFramework.Controls.MetroLabel metroLabel4;
            MetroFramework.Controls.MetroLabel metroLabel7;
            MetroFramework.Controls.MetroLabel metroLabel8;
            MetroFramework.Controls.MetroLabel metroLabel6;
            MetroFramework.Controls.MetroLabel metroLabel5;
            MetroFramework.Controls.MetroLabel metroLabel10;
            MetroFramework.Controls.MetroLabel metroLabel9;
            MetroFramework.Controls.MetroLabel metroLabel11;
            MetroFramework.Controls.MetroLabel metroLabel13;
            System.Windows.Forms.PictureBox pictureBox1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.HistoryKeyColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.HistoryStartDateColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.MainTabpages = new MetroFramework.Controls.MetroTabControl();
            this.RecentTab = new MetroFramework.Controls.MetroTabPage();
            this.RecentSearchPanel = new System.Windows.Forms.Panel();
            this.WorklogCommentPanel = new System.Windows.Forms.Panel();
            this.WorklogComment = new MetroFramework.Controls.MetroTextBox();
            this.AddIssueManuallyLink = new MetroFramework.Controls.MetroLink();
            this.RecentTileFilterInput = new MetroFramework.Controls.MetroTextBox();
            this.TilePanel = new System.Windows.Forms.Panel();
            this.HistoryTab = new MetroFramework.Controls.MetroTabPage();
            this.HistoryFilterPanel = new System.Windows.Forms.Panel();
            this.PushWorklogs = new MetroFramework.Controls.MetroButton();
            this.HistoryFilterPeriod = new System.Windows.Forms.ComboBox();
            this.HistoryFilterDate = new System.Windows.Forms.DateTimePicker();
            this.HistoryList = new BrightIdeasSoftware.ObjectListView();
            this.OptionsTab = new MetroFramework.Controls.MetroTabPage();
            this.AutoDetectSwitch = new MetroFramework.Controls.MetroToggle();
            this.JiraURLInput = new MetroFramework.Controls.MetroTextBox();
            this.JiraIntegrationHintLabel = new MetroFramework.Controls.MetroLabel();
            this.StylePicker = new MetroFramework.Controls.MetroComboBox();
            this.LoginSecretInput = new MetroFramework.Controls.MetroTextBox();
            this.LoginNameInput = new MetroFramework.Controls.MetroTextBox();
            this.RoundMinutesPicker = new MetroFramework.Controls.MetroTrackBar();
            this.RoundMinutesLabel = new MetroFramework.Controls.MetroLabel();
            this.DarkThemeSwitch = new MetroFramework.Controls.MetroToggle();
            this.BusySpinner = new MetroFramework.Controls.MetroProgressSpinner();
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.PeriodicTimer = new System.Windows.Forms.Timer(this.components);
            this.metroToolTip1 = new MetroFramework.Components.MetroToolTip();
            this.TimeLoggedTodayLabel = new MetroFramework.Controls.MetroLabel();
            this.TileContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.OpenInJIRAMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyJiraKeyMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.WatchClipboardTimer = new System.Windows.Forms.Timer(this.components);
            olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            metroLabel1 = new MetroFramework.Controls.MetroLabel();
            metroLabel2 = new MetroFramework.Controls.MetroLabel();
            HistoryApplyButton = new MetroFramework.Controls.MetroButton();
            metroLabel3 = new MetroFramework.Controls.MetroLabel();
            olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            metroLabel4 = new MetroFramework.Controls.MetroLabel();
            metroLabel7 = new MetroFramework.Controls.MetroLabel();
            metroLabel8 = new MetroFramework.Controls.MetroLabel();
            metroLabel6 = new MetroFramework.Controls.MetroLabel();
            metroLabel5 = new MetroFramework.Controls.MetroLabel();
            metroLabel10 = new MetroFramework.Controls.MetroLabel();
            metroLabel9 = new MetroFramework.Controls.MetroLabel();
            metroLabel11 = new MetroFramework.Controls.MetroLabel();
            metroLabel13 = new MetroFramework.Controls.MetroLabel();
            pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).BeginInit();
            this.MainTabpages.SuspendLayout();
            this.RecentTab.SuspendLayout();
            this.RecentSearchPanel.SuspendLayout();
            this.WorklogCommentPanel.SuspendLayout();
            this.HistoryTab.SuspendLayout();
            this.HistoryFilterPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryList)).BeginInit();
            this.OptionsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            this.TileContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // olvColumn3
            // 
            olvColumn3.AspectName = "Minutes";
            olvColumn3.Text = "Minutes Worked";
            olvColumn3.Width = 100;
            // 
            // olvColumn4
            // 
            olvColumn4.AspectName = "Comment";
            olvColumn4.Text = "Comment";
            olvColumn4.Width = 400;
            // 
            // metroLabel1
            // 
            metroLabel1.AutoSize = true;
            metroLabel1.Location = new System.Drawing.Point(9, 9);
            metroLabel1.Name = "metroLabel1";
            metroLabel1.Size = new System.Drawing.Size(63, 19);
            metroLabel1.TabIndex = 0;
            metroLabel1.Text = "Period of";
            // 
            // metroLabel2
            // 
            metroLabel2.AutoSize = true;
            metroLabel2.Location = new System.Drawing.Point(222, 9);
            metroLabel2.Name = "metroLabel2";
            metroLabel2.Size = new System.Drawing.Size(129, 19);
            metroLabel2.TabIndex = 3;
            metroLabel2.Text = "started on or before";
            // 
            // HistoryApplyButton
            // 
            HistoryApplyButton.Location = new System.Drawing.Point(574, 9);
            HistoryApplyButton.Name = "HistoryApplyButton";
            HistoryApplyButton.Size = new System.Drawing.Size(75, 23);
            HistoryApplyButton.TabIndex = 5;
            HistoryApplyButton.Text = "Apply";
            HistoryApplyButton.Click += new System.EventHandler(this.HistoryApplyButton_Click);
            // 
            // metroLabel3
            // 
            metroLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            metroLabel3.AutoSize = true;
            metroLabel3.FontSize = MetroFramework.MetroLabelSize.Small;
            metroLabel3.Location = new System.Drawing.Point(406, 9);
            metroLabel3.Name = "metroLabel3";
            metroLabel3.Size = new System.Drawing.Size(73, 15);
            metroLabel3.TabIndex = 2;
            metroLabel3.Text = "Search Issues";
            // 
            // olvColumn1
            // 
            olvColumn1.AspectName = "SyncedWithJira";
            olvColumn1.CheckBoxes = true;
            olvColumn1.IsEditable = false;
            olvColumn1.Text = "Synced";
            // 
            // metroLabel4
            // 
            metroLabel4.AutoSize = true;
            metroLabel4.Location = new System.Drawing.Point(25, 74);
            metroLabel4.Name = "metroLabel4";
            metroLabel4.Size = new System.Drawing.Size(80, 19);
            metroLabel4.TabIndex = 1;
            metroLabel4.Text = "Dark Theme";
            // 
            // metroLabel7
            // 
            metroLabel7.AutoSize = true;
            metroLabel7.FontSize = MetroFramework.MetroLabelSize.Tall;
            metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            metroLabel7.Location = new System.Drawing.Point(29, 29);
            metroLabel7.Name = "metroLabel7";
            metroLabel7.Size = new System.Drawing.Size(31, 25);
            metroLabel7.TabIndex = 0;
            metroLabel7.Text = "UI";
            metroLabel7.UseStyleColors = true;
            // 
            // metroLabel8
            // 
            metroLabel8.AutoSize = true;
            metroLabel8.FontSize = MetroFramework.MetroLabelSize.Tall;
            metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            metroLabel8.Location = new System.Drawing.Point(427, 29);
            metroLabel8.Name = "metroLabel8";
            metroLabel8.Size = new System.Drawing.Size(143, 25);
            metroLabel8.TabIndex = 8;
            metroLabel8.Text = "Jira Integration";
            metroLabel8.UseStyleColors = true;
            // 
            // metroLabel6
            // 
            metroLabel6.AutoSize = true;
            metroLabel6.Location = new System.Drawing.Point(427, 127);
            metroLabel6.Name = "metroLabel6";
            metroLabel6.Size = new System.Drawing.Size(63, 19);
            metroLabel6.TabIndex = 13;
            metroLabel6.Text = "Password";
            // 
            // metroLabel5
            // 
            metroLabel5.AutoSize = true;
            metroLabel5.Location = new System.Drawing.Point(427, 98);
            metroLabel5.Name = "metroLabel5";
            metroLabel5.Size = new System.Drawing.Size(41, 19);
            metroLabel5.TabIndex = 11;
            metroLabel5.Text = "Email";
            // 
            // metroLabel10
            // 
            metroLabel10.AutoSize = true;
            metroLabel10.Location = new System.Drawing.Point(427, 69);
            metroLabel10.Name = "metroLabel10";
            metroLabel10.Size = new System.Drawing.Size(48, 19);
            metroLabel10.TabIndex = 9;
            metroLabel10.Text = "Jira url";
            // 
            // metroLabel9
            // 
            metroLabel9.AutoSize = true;
            metroLabel9.Location = new System.Drawing.Point(25, 117);
            metroLabel9.Name = "metroLabel9";
            metroLabel9.Size = new System.Drawing.Size(36, 19);
            metroLabel9.TabIndex = 3;
            metroLabel9.Text = "Style";
            // 
            // metroLabel11
            // 
            metroLabel11.AutoSize = true;
            metroLabel11.FontSize = MetroFramework.MetroLabelSize.Tall;
            metroLabel11.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            metroLabel11.Location = new System.Drawing.Point(29, 214);
            metroLabel11.Name = "metroLabel11";
            metroLabel11.Size = new System.Drawing.Size(61, 25);
            metroLabel11.TabIndex = 5;
            metroLabel11.Text = "Timer";
            metroLabel11.UseStyleColors = true;
            // 
            // metroLabel13
            // 
            metroLabel13.AutoSize = true;
            metroLabel13.Location = new System.Drawing.Point(25, 349);
            metroLabel13.Name = "metroLabel13";
            metroLabel13.Size = new System.Drawing.Size(207, 19);
            metroLabel13.TabIndex = 16;
            metroLabel13.Text = "Watch the clipboard for issue keys";
            // 
            // pictureBox1
            // 
            pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            pictureBox1.Location = new System.Drawing.Point(589, 27);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new System.Drawing.Size(22, 22);
            pictureBox1.TabIndex = 9;
            pictureBox1.TabStop = false;
            // 
            // HistoryKeyColumn
            // 
            this.HistoryKeyColumn.AspectName = "Key";
            this.HistoryKeyColumn.Text = "Key";
            this.HistoryKeyColumn.Width = 90;
            // 
            // HistoryStartDateColumn
            // 
            this.HistoryStartDateColumn.AspectName = "Start";
            this.HistoryStartDateColumn.AspectToStringFormat = "{0:yyyy-MM-dd HH:mm}";
            this.HistoryStartDateColumn.Text = "Start Date";
            this.HistoryStartDateColumn.Width = 120;
            // 
            // MainTabpages
            // 
            this.MainTabpages.Controls.Add(this.RecentTab);
            this.MainTabpages.Controls.Add(this.HistoryTab);
            this.MainTabpages.Controls.Add(this.OptionsTab);
            this.MainTabpages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTabpages.Location = new System.Drawing.Point(20, 60);
            this.MainTabpages.Name = "MainTabpages";
            this.MainTabpages.SelectedIndex = 0;
            this.MainTabpages.Size = new System.Drawing.Size(894, 461);
            this.MainTabpages.TabIndex = 5;
            this.MainTabpages.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.MainTabpages_Selecting);
            // 
            // RecentTab
            // 
            this.RecentTab.Controls.Add(this.RecentSearchPanel);
            this.RecentTab.Controls.Add(this.TilePanel);
            this.RecentTab.HorizontalScrollbarBarColor = true;
            this.RecentTab.HorizontalScrollbarSize = 13;
            this.RecentTab.Location = new System.Drawing.Point(4, 35);
            this.RecentTab.Name = "RecentTab";
            this.RecentTab.Size = new System.Drawing.Size(886, 422);
            this.RecentTab.TabIndex = 0;
            this.RecentTab.Text = "Recent";
            this.RecentTab.VerticalScrollbarBarColor = true;
            this.RecentTab.VerticalScrollbarSize = 12;
            // 
            // RecentSearchPanel
            // 
            this.RecentSearchPanel.BackColor = System.Drawing.Color.Transparent;
            this.RecentSearchPanel.Controls.Add(this.WorklogCommentPanel);
            this.RecentSearchPanel.Controls.Add(this.AddIssueManuallyLink);
            this.RecentSearchPanel.Controls.Add(metroLabel3);
            this.RecentSearchPanel.Controls.Add(this.RecentTileFilterInput);
            this.RecentSearchPanel.Controls.Add(pictureBox1);
            this.RecentSearchPanel.Location = new System.Drawing.Point(132, 39);
            this.RecentSearchPanel.Name = "RecentSearchPanel";
            this.RecentSearchPanel.Size = new System.Drawing.Size(614, 72);
            this.RecentSearchPanel.TabIndex = 10;
            // 
            // WorklogCommentPanel
            // 
            this.WorklogCommentPanel.Controls.Add(this.WorklogComment);
            this.WorklogCommentPanel.Location = new System.Drawing.Point(0, 2);
            this.WorklogCommentPanel.Name = "WorklogCommentPanel";
            this.WorklogCommentPanel.Size = new System.Drawing.Size(300, 70);
            this.WorklogCommentPanel.TabIndex = 10;
            // 
            // WorklogComment
            // 
            this.WorklogComment.Location = new System.Drawing.Point(3, 25);
            this.WorklogComment.Multiline = true;
            this.WorklogComment.Name = "WorklogComment";
            this.WorklogComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.WorklogComment.Size = new System.Drawing.Size(165, 42);
            this.WorklogComment.TabIndex = 1;
            this.WorklogComment.UseStyleColors = true;
            this.WorklogComment.Enter += new System.EventHandler(this.WorklogComment_Enter);
            this.WorklogComment.Leave += new System.EventHandler(this.WorklogComment_Leave);
            this.WorklogComment.Validated += new System.EventHandler(this.WorklogComment_Validated);
            // 
            // AddIssueManuallyLink
            // 
            this.AddIssueManuallyLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AddIssueManuallyLink.FontWeight = MetroFramework.MetroLinkWeight.Light;
            this.AddIssueManuallyLink.Location = new System.Drawing.Point(406, 53);
            this.AddIssueManuallyLink.Name = "AddIssueManuallyLink";
            this.AddIssueManuallyLink.Size = new System.Drawing.Size(177, 17);
            this.AddIssueManuallyLink.TabIndex = 4;
            this.AddIssueManuallyLink.Text = "Add Issue by key";
            this.AddIssueManuallyLink.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AddIssueManuallyLink.Click += new System.EventHandler(this.AddIssueManuallyLink_Click);
            // 
            // RecentTileFilterInput
            // 
            this.RecentTileFilterInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RecentTileFilterInput.Location = new System.Drawing.Point(406, 27);
            this.RecentTileFilterInput.MaxLength = 42;
            this.RecentTileFilterInput.Name = "RecentTileFilterInput";
            this.RecentTileFilterInput.PromptText = "search";
            this.RecentTileFilterInput.Size = new System.Drawing.Size(177, 23);
            this.RecentTileFilterInput.TabIndex = 3;
            this.RecentTileFilterInput.UseStyleColors = true;
            this.RecentTileFilterInput.TextChanged += new System.EventHandler(this.RecentTileFilterInput_TextChanged);
            // 
            // TilePanel
            // 
            this.TilePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TilePanel.BackColor = System.Drawing.Color.Transparent;
            this.TilePanel.Location = new System.Drawing.Point(132, 133);
            this.TilePanel.Name = "TilePanel";
            this.TilePanel.Size = new System.Drawing.Size(0, 0);
            this.TilePanel.TabIndex = 5;
            this.TilePanel.Resize += new System.EventHandler(this.TilePanel_Resize);
            // 
            // HistoryTab
            // 
            this.HistoryTab.Controls.Add(this.HistoryFilterPanel);
            this.HistoryTab.Controls.Add(this.HistoryList);
            this.HistoryTab.HorizontalScrollbarBarColor = true;
            this.HistoryTab.HorizontalScrollbarSize = 13;
            this.HistoryTab.Location = new System.Drawing.Point(4, 35);
            this.HistoryTab.Name = "HistoryTab";
            this.HistoryTab.Padding = new System.Windows.Forms.Padding(12, 13, 12, 13);
            this.HistoryTab.Size = new System.Drawing.Size(886, 422);
            this.HistoryTab.TabIndex = 1;
            this.HistoryTab.Text = "History";
            this.HistoryTab.VerticalScrollbarBarColor = true;
            this.HistoryTab.VerticalScrollbarSize = 12;
            // 
            // HistoryFilterPanel
            // 
            this.HistoryFilterPanel.BackColor = System.Drawing.Color.Transparent;
            this.HistoryFilterPanel.Controls.Add(this.PushWorklogs);
            this.HistoryFilterPanel.Controls.Add(HistoryApplyButton);
            this.HistoryFilterPanel.Controls.Add(this.HistoryFilterPeriod);
            this.HistoryFilterPanel.Controls.Add(metroLabel2);
            this.HistoryFilterPanel.Controls.Add(this.HistoryFilterDate);
            this.HistoryFilterPanel.Controls.Add(metroLabel1);
            this.HistoryFilterPanel.Location = new System.Drawing.Point(12, 38);
            this.HistoryFilterPanel.Name = "HistoryFilterPanel";
            this.HistoryFilterPanel.Size = new System.Drawing.Size(862, 38);
            this.HistoryFilterPanel.TabIndex = 3;
            // 
            // PushWorklogs
            // 
            this.PushWorklogs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PushWorklogs.Highlight = true;
            this.PushWorklogs.Location = new System.Drawing.Point(759, 9);
            this.PushWorklogs.Name = "PushWorklogs";
            this.PushWorklogs.Size = new System.Drawing.Size(100, 23);
            this.PushWorklogs.TabIndex = 6;
            this.PushWorklogs.Text = "Push to JIRA";
            this.PushWorklogs.Click += new System.EventHandler(this.PushWorklogs_Click);
            // 
            // HistoryFilterPeriod
            // 
            this.HistoryFilterPeriod.FormattingEnabled = true;
            this.HistoryFilterPeriod.ItemHeight = 13;
            this.HistoryFilterPeriod.Items.AddRange(new object[] {
            "1 Week",
            "1 Month",
            "3 Months",
            "6 Months",
            "1 Year"});
            this.HistoryFilterPeriod.Location = new System.Drawing.Point(78, 9);
            this.HistoryFilterPeriod.Name = "HistoryFilterPeriod";
            this.HistoryFilterPeriod.Size = new System.Drawing.Size(138, 21);
            this.HistoryFilterPeriod.TabIndex = 4;
            this.HistoryFilterPeriod.Validated += new System.EventHandler(this.HistoryFilterPeriod_Validated);
            // 
            // HistoryFilterDate
            // 
            this.HistoryFilterDate.CustomFormat = "yyy-MM-dd";
            this.HistoryFilterDate.Location = new System.Drawing.Point(357, 9);
            this.HistoryFilterDate.Name = "HistoryFilterDate";
            this.HistoryFilterDate.Size = new System.Drawing.Size(200, 20);
            this.HistoryFilterDate.TabIndex = 2;
            this.HistoryFilterDate.Validated += new System.EventHandler(this.HistoryFilterDate_Validated);
            // 
            // HistoryList
            // 
            this.HistoryList.AllColumns.Add(this.HistoryKeyColumn);
            this.HistoryList.AllColumns.Add(olvColumn1);
            this.HistoryList.AllColumns.Add(this.HistoryStartDateColumn);
            this.HistoryList.AllColumns.Add(olvColumn3);
            this.HistoryList.AllColumns.Add(olvColumn4);
            this.HistoryList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.HistoryList.CellEditUseWholeCell = false;
            this.HistoryList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.HistoryKeyColumn,
            olvColumn1,
            this.HistoryStartDateColumn,
            olvColumn3,
            olvColumn4});
            this.HistoryList.Cursor = System.Windows.Forms.Cursors.Default;
            this.HistoryList.FullRowSelect = true;
            this.HistoryList.Location = new System.Drawing.Point(12, 120);
            this.HistoryList.Name = "HistoryList";
            this.HistoryList.Size = new System.Drawing.Size(862, 289);
            this.HistoryList.TabIndex = 2;
            this.HistoryList.TintSortColumn = true;
            this.HistoryList.UseCompatibleStateImageBehavior = false;
            this.HistoryList.View = System.Windows.Forms.View.Details;
            // 
            // OptionsTab
            // 
            this.OptionsTab.Controls.Add(this.AutoDetectSwitch);
            this.OptionsTab.Controls.Add(metroLabel13);
            this.OptionsTab.Controls.Add(metroLabel11);
            this.OptionsTab.Controls.Add(this.JiraURLInput);
            this.OptionsTab.Controls.Add(metroLabel10);
            this.OptionsTab.Controls.Add(this.JiraIntegrationHintLabel);
            this.OptionsTab.Controls.Add(this.StylePicker);
            this.OptionsTab.Controls.Add(metroLabel9);
            this.OptionsTab.Controls.Add(metroLabel8);
            this.OptionsTab.Controls.Add(metroLabel7);
            this.OptionsTab.Controls.Add(this.LoginSecretInput);
            this.OptionsTab.Controls.Add(metroLabel6);
            this.OptionsTab.Controls.Add(this.LoginNameInput);
            this.OptionsTab.Controls.Add(metroLabel5);
            this.OptionsTab.Controls.Add(this.RoundMinutesPicker);
            this.OptionsTab.Controls.Add(this.RoundMinutesLabel);
            this.OptionsTab.Controls.Add(metroLabel4);
            this.OptionsTab.Controls.Add(this.DarkThemeSwitch);
            this.OptionsTab.HorizontalScrollbarBarColor = true;
            this.OptionsTab.HorizontalScrollbarSize = 13;
            this.OptionsTab.Location = new System.Drawing.Point(4, 35);
            this.OptionsTab.Name = "OptionsTab";
            this.OptionsTab.Size = new System.Drawing.Size(886, 422);
            this.OptionsTab.TabIndex = 2;
            this.OptionsTab.Text = "Options";
            this.OptionsTab.VerticalScrollbarBarColor = true;
            this.OptionsTab.VerticalScrollbarSize = 12;
            // 
            // AutoDetectSwitch
            // 
            this.AutoDetectSwitch.AutoSize = true;
            this.AutoDetectSwitch.Location = new System.Drawing.Point(131, 371);
            this.AutoDetectSwitch.Name = "AutoDetectSwitch";
            this.AutoDetectSwitch.Size = new System.Drawing.Size(80, 17);
            this.AutoDetectSwitch.TabIndex = 17;
            this.AutoDetectSwitch.Text = "Off";
            this.metroToolTip1.SetToolTip(this.AutoDetectSwitch, "Use regular expressions to watch for issue keys on your clipboard.");
            this.AutoDetectSwitch.UseVisualStyleBackColor = true;
            this.AutoDetectSwitch.CheckedChanged += new System.EventHandler(this.AutoDetectSwitch_CheckedChanged);
            // 
            // JiraURLInput
            // 
            this.JiraURLInput.Location = new System.Drawing.Point(516, 65);
            this.JiraURLInput.Name = "JiraURLInput";
            this.JiraURLInput.Size = new System.Drawing.Size(218, 23);
            this.JiraURLInput.TabIndex = 10;
            this.JiraURLInput.Validated += new System.EventHandler(this.JiraURLInput_Validated);
            // 
            // JiraIntegrationHintLabel
            // 
            this.JiraIntegrationHintLabel.AutoSize = true;
            this.JiraIntegrationHintLabel.Location = new System.Drawing.Point(516, 149);
            this.JiraIntegrationHintLabel.Name = "JiraIntegrationHintLabel";
            this.JiraIntegrationHintLabel.Size = new System.Drawing.Size(189, 19);
            this.JiraIntegrationHintLabel.TabIndex = 15;
            this.JiraIntegrationHintLabel.Text = "Password encrypted and saved";
            this.JiraIntegrationHintLabel.UseStyleColors = true;
            this.JiraIntegrationHintLabel.Visible = false;
            // 
            // StylePicker
            // 
            this.StylePicker.FormattingEnabled = true;
            this.StylePicker.ItemHeight = 23;
            this.StylePicker.Items.AddRange(new object[] {
            "Default",
            "Black",
            "White",
            "Silver",
            "Blue",
            "Green",
            "Lime",
            "Teal",
            "Orange",
            "Brown",
            "Pink",
            "Magenta",
            "Purple",
            "Red",
            "Yellow"});
            this.StylePicker.Location = new System.Drawing.Point(29, 139);
            this.StylePicker.Name = "StylePicker";
            this.StylePicker.Size = new System.Drawing.Size(186, 29);
            this.StylePicker.TabIndex = 4;
            this.StylePicker.SelectedIndexChanged += new System.EventHandler(this.StylePicker_SelectedIndexChanged);
            // 
            // LoginSecretInput
            // 
            this.LoginSecretInput.Location = new System.Drawing.Point(516, 123);
            this.LoginSecretInput.Name = "LoginSecretInput";
            this.LoginSecretInput.PasswordChar = '●';
            this.LoginSecretInput.Size = new System.Drawing.Size(218, 23);
            this.LoginSecretInput.TabIndex = 14;
            this.LoginSecretInput.UseSystemPasswordChar = true;
            this.LoginSecretInput.Enter += new System.EventHandler(this.LoginSecretInput_Enter);
            this.LoginSecretInput.Validated += new System.EventHandler(this.LoginSecretInput_Validated);
            // 
            // LoginNameInput
            // 
            this.LoginNameInput.Location = new System.Drawing.Point(516, 94);
            this.LoginNameInput.Name = "LoginNameInput";
            this.LoginNameInput.Size = new System.Drawing.Size(218, 23);
            this.LoginNameInput.TabIndex = 12;
            this.LoginNameInput.Validated += new System.EventHandler(this.LoginNameInput_Validated);
            // 
            // RoundMinutesPicker
            // 
            this.RoundMinutesPicker.BackColor = System.Drawing.Color.Transparent;
            this.RoundMinutesPicker.Location = new System.Drawing.Point(29, 285);
            this.RoundMinutesPicker.Maximum = 30;
            this.RoundMinutesPicker.Name = "RoundMinutesPicker";
            this.RoundMinutesPicker.Size = new System.Drawing.Size(182, 22);
            this.RoundMinutesPicker.SmallChange = 5;
            this.RoundMinutesPicker.TabIndex = 7;
            this.RoundMinutesPicker.Text = "metroTrackBar1";
            this.RoundMinutesPicker.Value = 0;
            this.RoundMinutesPicker.ValueChanged += new System.EventHandler(this.RoundMinutesPicker_ValueChanged);
            // 
            // RoundMinutesLabel
            // 
            this.RoundMinutesLabel.AutoSize = true;
            this.RoundMinutesLabel.Location = new System.Drawing.Point(25, 263);
            this.RoundMinutesLabel.Name = "RoundMinutesLabel";
            this.RoundMinutesLabel.Size = new System.Drawing.Size(108, 19);
            this.RoundMinutesLabel.TabIndex = 6;
            this.RoundMinutesLabel.Text = "Round up n mins";
            // 
            // DarkThemeSwitch
            // 
            this.DarkThemeSwitch.AutoSize = true;
            this.DarkThemeSwitch.Location = new System.Drawing.Point(131, 76);
            this.DarkThemeSwitch.Name = "DarkThemeSwitch";
            this.DarkThemeSwitch.Size = new System.Drawing.Size(80, 17);
            this.DarkThemeSwitch.TabIndex = 2;
            this.DarkThemeSwitch.Text = "Off";
            this.DarkThemeSwitch.UseVisualStyleBackColor = true;
            this.DarkThemeSwitch.CheckedChanged += new System.EventHandler(this.DarkThemeSwitch_CheckedChanged);
            // 
            // BusySpinner
            // 
            this.BusySpinner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BusySpinner.Location = new System.Drawing.Point(882, 33);
            this.BusySpinner.Maximum = 100;
            this.BusySpinner.Name = "BusySpinner";
            this.BusySpinner.Size = new System.Drawing.Size(32, 32);
            this.BusySpinner.TabIndex = 13;
            this.BusySpinner.Value = 50;
            this.BusySpinner.Visible = false;
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = this;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // PeriodicTimer
            // 
            this.PeriodicTimer.Interval = 30000;
            this.PeriodicTimer.Tick += new System.EventHandler(this.PeriodicTimer_Tick);
            // 
            // TimeLoggedTodayLabel
            // 
            this.TimeLoggedTodayLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TimeLoggedTodayLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.TimeLoggedTodayLabel.Location = new System.Drawing.Point(628, 33);
            this.TimeLoggedTodayLabel.Name = "TimeLoggedTodayLabel";
            this.TimeLoggedTodayLabel.Size = new System.Drawing.Size(231, 23);
            this.TimeLoggedTodayLabel.TabIndex = 14;
            this.TimeLoggedTodayLabel.Text = "Worked n hours today";
            this.TimeLoggedTodayLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.metroToolTip1.SetToolTip(this.TimeLoggedTodayLabel, "* Only time tracked in Ti.me is totalled here");
            // 
            // TileContextMenu
            // 
            this.TileContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenInJIRAMenu,
            this.CopyJiraKeyMenu});
            this.TileContextMenu.Name = "TileContextMenu";
            this.TileContextMenu.Size = new System.Drawing.Size(164, 48);
            // 
            // OpenInJIRAMenu
            // 
            this.OpenInJIRAMenu.Image = global::Ti.Me.Properties.Resources.internet_web_browser;
            this.OpenInJIRAMenu.Name = "OpenInJIRAMenu";
            this.OpenInJIRAMenu.Size = new System.Drawing.Size(163, 22);
            this.OpenInJIRAMenu.Text = "&Open in Jira";
            this.OpenInJIRAMenu.Click += new System.EventHandler(this.OpenInJIRAMenu_Click);
            // 
            // CopyJiraKeyMenu
            // 
            this.CopyJiraKeyMenu.Image = global::Ti.Me.Properties.Resources.edit_copy;
            this.CopyJiraKeyMenu.Name = "CopyJiraKeyMenu";
            this.CopyJiraKeyMenu.Size = new System.Drawing.Size(163, 22);
            this.CopyJiraKeyMenu.Text = "&Copy the Jira key";
            this.CopyJiraKeyMenu.Click += new System.EventHandler(this.CopyJiraKeyMenu_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Ti.me";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClicked);
            this.notifyIcon1.Click += new System.EventHandler(this.notifyIcon1_Click);
            // 
            // WatchClipboardTimer
            // 
            this.WatchClipboardTimer.Interval = 5000;
            this.WatchClipboardTimer.Tick += new System.EventHandler(this.WatchClipboardTimer_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 541);
            this.Controls.Add(this.TimeLoggedTodayLabel);
            this.Controls.Add(this.BusySpinner);
            this.Controls.Add(this.MainTabpages);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Main";
            this.Text = "Ti.me";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).EndInit();
            this.MainTabpages.ResumeLayout(false);
            this.RecentTab.ResumeLayout(false);
            this.RecentSearchPanel.ResumeLayout(false);
            this.RecentSearchPanel.PerformLayout();
            this.WorklogCommentPanel.ResumeLayout(false);
            this.HistoryTab.ResumeLayout(false);
            this.HistoryFilterPanel.ResumeLayout(false);
            this.HistoryFilterPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryList)).EndInit();
            this.OptionsTab.ResumeLayout(false);
            this.OptionsTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.TileContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl MainTabpages;
        private MetroFramework.Controls.MetroTabPage RecentTab;
        private MetroFramework.Controls.MetroTabPage HistoryTab;
        private BrightIdeasSoftware.ObjectListView HistoryList;
        private MetroFramework.Controls.MetroTabPage OptionsTab;
        private System.Windows.Forms.Panel TilePanel;
        private MetroFramework.Controls.MetroTextBox RecentTileFilterInput;
        private BrightIdeasSoftware.OLVColumn HistoryKeyColumn;
        private BrightIdeasSoftware.OLVColumn HistoryStartDateColumn;
        private System.Windows.Forms.Panel RecentSearchPanel;
        private System.Windows.Forms.Panel HistoryFilterPanel;
        private System.Windows.Forms.DateTimePicker HistoryFilterDate;
        private System.Windows.Forms.ComboBox HistoryFilterPeriod;
        private MetroFramework.Controls.MetroButton PushWorklogs;
        private MetroFramework.Controls.MetroProgressSpinner BusySpinner;
        private MetroFramework.Controls.MetroToggle DarkThemeSwitch;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private System.Windows.Forms.ImageList imageList1;
        private MetroFramework.Controls.MetroTrackBar RoundMinutesPicker;
        private MetroFramework.Controls.MetroLabel RoundMinutesLabel;
        private MetroFramework.Controls.MetroComboBox StylePicker;
        private MetroFramework.Controls.MetroTextBox LoginSecretInput;
        private MetroFramework.Controls.MetroTextBox LoginNameInput;
        private MetroFramework.Controls.MetroLabel JiraIntegrationHintLabel;
        private MetroFramework.Controls.MetroTextBox JiraURLInput;
        private System.Windows.Forms.Timer PeriodicTimer;
        private MetroFramework.Controls.MetroLink AddIssueManuallyLink;
        private MetroFramework.Controls.MetroTextBox WorklogComment;
        private MetroFramework.Controls.MetroToggle AutoDetectSwitch;
        private MetroFramework.Components.MetroToolTip metroToolTip1;
        private System.Windows.Forms.ContextMenuStrip TileContextMenu;
        private System.Windows.Forms.ToolStripMenuItem OpenInJIRAMenu;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Timer WatchClipboardTimer;
        private System.Windows.Forms.Panel WorklogCommentPanel;
        private MetroFramework.Controls.MetroLabel TimeLoggedTodayLabel;
        private System.Windows.Forms.ToolStripMenuItem CopyJiraKeyMenu;
    }
}