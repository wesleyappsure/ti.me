﻿namespace Ti.Me.Forms
{
    partial class EnterIssueKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            MetroFramework.Controls.MetroLabel metroLabel1;
            this.IssueKeyInput = new MetroFramework.Controls.MetroTextBox();
            this.SubmitButton = new MetroFramework.Controls.MetroButton();
            this.AutoDetechHint = new MetroFramework.Controls.MetroLabel();
            metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            metroLabel1.AutoSize = true;
            metroLabel1.Location = new System.Drawing.Point(46, 72);
            metroLabel1.Name = "metroLabel1";
            metroLabel1.Size = new System.Drawing.Size(29, 19);
            metroLabel1.TabIndex = 0;
            metroLabel1.Text = "Key";
            // 
            // IssueKeyInput
            // 
            this.IssueKeyInput.Location = new System.Drawing.Point(81, 72);
            this.IssueKeyInput.Name = "IssueKeyInput";
            this.IssueKeyInput.Size = new System.Drawing.Size(155, 23);
            this.IssueKeyInput.TabIndex = 1;
            // 
            // SubmitButton
            // 
            this.SubmitButton.Highlight = true;
            this.SubmitButton.Location = new System.Drawing.Point(255, 72);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(107, 23);
            this.SubmitButton.TabIndex = 2;
            this.SubmitButton.Text = "Start Tracking";
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // AutoDetechHint
            // 
            this.AutoDetechHint.AutoSize = true;
            this.AutoDetechHint.Location = new System.Drawing.Point(81, 98);
            this.AutoDetechHint.Name = "AutoDetechHint";
            this.AutoDetechHint.Size = new System.Drawing.Size(131, 19);
            this.AutoDetechHint.TabIndex = 3;
            this.AutoDetechHint.Text = "From your clipboard";
            this.AutoDetechHint.Visible = false;
            // 
            // EnterIssueKey
            // 
            this.AcceptButton = this.SubmitButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(414, 132);
            this.Controls.Add(this.AutoDetechHint);
            this.Controls.Add(this.SubmitButton);
            this.Controls.Add(this.IssueKeyInput);
            this.Controls.Add(metroLabel1);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EnterIssueKey";
            this.Resizable = false;
            this.Text = "Track Issue";
            this.Load += new System.EventHandler(this.EnterIssueKey_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EnterIssueKey_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox IssueKeyInput;
        private MetroFramework.Controls.MetroButton SubmitButton;
        private MetroFramework.Controls.MetroLabel AutoDetechHint;
    }
}