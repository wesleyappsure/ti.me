﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ti.Me.Forms
{
    public partial class EnterIssueKey : MetroFramework.Forms.MetroForm
    {
        public string IssueKey { get; set; }
        public Model.Options options { get; set; }

        public EnterIssueKey()
        {
            InitializeComponent();
        }

        private void EnterIssueKey_Load(object sender, EventArgs e)
        {
            if (options.AutoDetectFromClipboard)
            {
                var clip = Clipboard.GetText(TextDataFormat.UnicodeText);
                if (!string.IsNullOrEmpty(clip))
                {
                    var db = new Data.Database();
                    var projectList = db.Projects(n => true).Result;
                    foreach (var project in projectList)
                    {
                        // build the regex to find the project key
                        var expression = string.Format(@"(\b{0}-\d*)", project.Key);
                        var match = Regex.Match(clip, expression);
                        if (match.Success)
                        {
                            IssueKeyInput.Text = match.Value;
                            AutoDetechHint.Show();
                        }
                    }
                }
            }
        }
        private void SubmitButton_Click(object sender, EventArgs e)
        {
            IssueKey = IssueKeyInput.Text;
            if (!string.IsNullOrEmpty(IssueKey))
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void EnterIssueKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
        }


    }
}
