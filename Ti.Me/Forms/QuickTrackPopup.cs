﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ti.Me.Forms
{
    public partial class QuickTrackPopup : MetroFramework.Forms.MetroForm
    {
        public Model.Issue issue { get; set; }

        public QuickTrackPopup()
        {
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
