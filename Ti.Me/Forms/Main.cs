﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ti.Me.Logic;

namespace Ti.Me.Forms
{
    public partial class Main : MetroFramework.Forms.MetroForm
    {

        /// <summary>
        /// Tracks running animations, used to cancel animations when filtering.
        /// </summary>
        private List<MetroFramework.Animation.MoveAnimation> animations = new List<MetroFramework.Animation.MoveAnimation>();

        /// <summary>
        /// The currently running time tracker. Null if nothing is tracking.
        /// </summary>
        private Model.Tracker currentRunningTracker;

        /// <summary>
        /// The user options.
        /// </summary>
        private Model.Options options;

        /// <summary>
        /// Tracks the worklog animation.
        /// </summary>
        private MetroFramework.Animation.MoveAnimation WorklogAnimation;
        private readonly string CommentPlaceholder = "worklog comments";

        public Main()
        {
            InitializeComponent();
            ConfigureUI();
            LoadOptions();
            SyncRecentIssues();
            SyncProjects();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (currentRunningTracker != null)
            {
                var tc = new TrackerController();
                await tc.EndTracking(currentRunningTracker, options.RoundMinutesUp);
                SyncWorkLogs();
            }
            new Data.Database().UpsertOptions(options);
        }

        /// <summary>
        /// Configure the main UI.
        /// </summary>
        private async void ConfigureUI()
        {
            BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            RecentSearchPanel.Dock = DockStyle.Bottom;
            TilePanel.BringToFront();
            TilePanel.Dock = DockStyle.Fill;
            HistoryList.Dock = DockStyle.Fill;
            HistoryList.BringToFront();
            HistoryFilterPanel.Dock = DockStyle.Bottom;
            MainTabpages.SelectedTab = RecentTab;
            HistoryFilterPeriod.SelectedIndex = 1;
            TimeLoggedTodayLabel.Text = await GetTotalTimeWorkedToday();
            // hide the worklog comment panel
            WorklogComment.Dock = DockStyle.Fill;
            WorklogCommentPanel.Left = -WorklogCommentPanel.Width;
            WorklogComment.CustomForeColor = true;
            // start the timer to periodically update the currently tracking issue display
            PeriodicTimer.Start();
            WatchClipboardTimer.Start();
        }


        /// <summary>
        /// Loads the user options and applies the saved values.
        /// </summary>
        private void LoadOptions()
        {
            options = new Data.Database().GetOptions();
            DarkThemeSwitch.Checked = options.Dark;
            RoundMinutesPicker.Value = options.RoundMinutesUp;
            StylePicker.SelectedIndex = options.StyleIndex + 1;
            LoginNameInput.Text = options.LoginName;
            JiraURLInput.Text = options.JiraURL;
            AutoDetectSwitch.Checked = options.AutoDetectFromClipboard;
        }

        /// <summary>
        /// Fetch recently updated issues assigned to the user, for local caching and to speed up searching.
        /// </summary>
        private async void SyncRecentIssues()
        {
            BusySpinner.Show();
            try
            {
                var ji = new Data.JiraIntegration(options);
                await ji.SyncRecentIssues(options.LoginName);
            }
            catch (AggregateException ex)
            {
                MessageBox.Show("There was a problem fetching your recent jira issues. Check your login details.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                BusySpinner.Hide();
                ArrangeTiles(RecentTileFilterInput.Text);
            }
        }

        /// <summary>
        /// Fetch and cache jira project names and keys.
        /// </summary>
        private async void SyncProjects()
        {
            BusySpinner.Show();
            try
            {
                var ji = new Data.JiraIntegration(options);
                await ji.SyncProjects();
            }
            catch (AggregateException ex)
            {
                MessageBox.Show("There was a problem fetching jira projects. Check your login details.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                BusySpinner.Hide();
            }
        }

        /// <summary>
        /// Find a jira issue by key and cache the result to the local database for future use.
        /// </summary>
        /// <param name="key"></param>
        private async void SearchIssueOnJira(string key)
        {
            BusySpinner.Show();
            try
            {
                var ji = new Data.JiraIntegration(options);
                var issue = await ji.Search(key);
                if (issue != null)
                {
                    // store issue details in our local db
                    var db = new Data.Database();
                    await db.UpsertIssue(issue);
                }
            }
            catch (AggregateException ex)
            {
                MessageBox.Show("There was a problem fetching your recent jira issues. Check your login details.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                BusySpinner.Hide();
                ArrangeTiles(key, true);
            }
        }

        /// <summary>
        /// Show a dialog to input a jira key, and start tracking on the issue on accept.
        /// The issue details are looked up in jira automatically.
        /// </summary>
        private async void PromptForIssueKeyInput()
        {
            try
            {
                var prompt = new EnterIssueKey();
                prompt.options = options;
                if (prompt.ShowDialog(this) == DialogResult.OK)
                {
                    BusySpinner.Show();
                    TrackFromKey(prompt.IssueKey);
                }
            }
            catch (AggregateException ex)
            {
                MessageBox.Show("There was a problem fetching your recent jira issues. Check your login details.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                BusySpinner.Hide();
            }
        }

        /// <summary>
        /// Place the issue tiles on the screen.
        /// </summary>
        /// <param name="filter"></param>
        private async void ArrangeTiles(string filter, bool preventRecursiveSearching = false)
        {
            try
            {
                const int animationTime = 20;
                const int tileWidth = 185;
                const int tileHeight = 100;
                const int padding = 10;
                int x = padding;
                int y = padding;
                int xoffset = 1;

                var tc = new TrackerController();
                var issues = await tc.RecentIssues(filter, 12);

                // track tiles processed, used to identify tiles from previous searches to clear afterwards
                var processedTiles = new List<MetroFramework.Controls.MetroTile>();

                // stop ongoing animations
                animations.ForEach(n => n.Cancel());
                animations.Clear();

                foreach (var issue in issues)
                {
                    var tile = TilePanel.Controls.Find(issue.Key.ToString(), false).FirstOrDefault() as MetroFramework.Controls.MetroTile;

                    if (tile == null)
                    {
                        // add a new tile
                        tile = new MetroFramework.Controls.MetroTile();
                        tile.Size = new Size(tileWidth, tileHeight);
                        tile.Location = new Point(-tileWidth, TilePanel.Height / 2);
                        tile.Text = GetTileText(issue);
                        tile.Name = issue.Key.ToString().ToUpper();
                        tile.TileTextFontSize = MetroFramework.MetroTileTextSize.Medium;
                        tile.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
                        tile.PaintTileCount = true;
                        tile.TileImageAlign = ContentAlignment.TopRight;
                        tile.ContextMenuStrip = TileContextMenu;
                        tile.Click += Tile_Click;
                        TilePanel.Controls.Add(tile);
                    }
                    else
                    {
                        // update tile
                        tile.TileCount = 0;
                        tile.Text = GetTileText(issue);
                        tile.UseTileImage = false;
                    }

                    // hilight active
                    if (currentRunningTracker != null)
                    {
                        if (issue.Key == currentRunningTracker.Key)
                        {
                            // cycle the active tile style
                            tile.Style = (MetroFramework.MetroColorStyle)((options.StyleIndex + 5) % 13);
                            tile.TileCount = currentRunningTracker.Minutes;
                            // set a tile image
                            tile.TileImage = Properties.Resources.media_record;
                            tile.UseTileImage = true;
                        }
                        else
                        {
                            tile.Style = MetroFramework.MetroColorStyle.Default;
                        }
                    }
                    else tile.Style = MetroFramework.MetroColorStyle.Default;

                    // skip tiles without any size (these are hiding/hidden from the filter)
                    if (!tile.Size.IsEmpty)
                    {

                        // move to front
                        tile.SendToBack();

                        // animate
                        var ma = new MetroFramework.Animation.MoveAnimation();
                        ma.Start(tile, new Point(x, y), MetroFramework.Animation.TransitionType.EaseInCubic, animationTime);
                        animations.Add(ma);

                        // calculate next tile position
                        x = x + ((tileWidth + padding) * xoffset);
                        if (x > (TilePanel.Width - tileWidth - padding))
                        {
                            // move to next row
                            y = y + tileHeight + padding;
                            // shift back one space
                            x = x - tileWidth - padding;
                            // flip direction
                            xoffset = -xoffset;
                        }

                        // line up left to right
                        if (x < padding)
                        {
                            // move to next row
                            y = y + tileHeight + padding;
                            // shift forward one space
                            x = x + tileWidth + padding;
                            // flip direction
                            xoffset = -xoffset;
                        }

                        // line up top down
                        //y = y + tileHeight + padding;
                        //if (y > (TilePanel.Height - tileHeight - padding))
                        //{
                        //    x = x + tileWidth + padding;
                        //    y = padding;
                        //}

                        // track tiles processed
                        processedTiles.Add(tile);
                    }
                }

                // remove tiles not in the processed list
                foreach (Control control in TilePanel.Controls)
                {
                    if (!processedTiles.Contains(control))
                    {
                        // animate size
                        var ma = new MetroFramework.Animation.ExpandAnimation();
                        ma.Start(control, new Size(0, 0), MetroFramework.Animation.TransitionType.EaseInExpo, animationTime);
                        ma.AnimationCompleted += Ma_AnimationCompleted;
                        // unlink key to avoid filtering finding it
                        control.Name = "";
                    }
                }

                // search on jira
                if (issues.Count() == 0 && preventRecursiveSearching == false)
                {
                    SearchIssueOnJira(filter);
                    AddIssueManuallyLink.UseStyleColors = true;
                }
                else
                {
                    AddIssueManuallyLink.UseStyleColors = false;
                }

                // apply the chosen style to the tiles
                metroStyleManager1.Update();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// Gets the text of an issue for display on a tile.
        /// Long text is wrapped with newline characters.
        /// </summary>
        /// <param name="issue"></param>
        /// <returns></returns>
        private static string GetTileText(Model.Issue issue)
        {
            const int charPerLine = 20;

            var timeSpent = string.Empty;
            if (!string.IsNullOrEmpty(issue.TimeSpent))
            {
                timeSpent = string.Format("\n({0})", issue.TimeSpent);
            }

            if (issue.Summary == null)
            {
                return issue.Key ?? "NA";
            }
            else if (issue.Summary.Length <= charPerLine)
            {
                return string.Format("{0}\n{1}{2}", issue.Key, issue.Summary, timeSpent);
            }
            else
            {
                // Limit tiles to n lines of summary.
                var lines = 0;
                var summary = issue.Summary;
                var words = new List<string>();
                while (summary.Length > 0 && lines < 3)
                {
                    lines++;
                    if (summary.Length > charPerLine)
                    {
                        var nextSplit = summary.IndexOf(' ', charPerLine);
                        if (nextSplit == -1)
                        {
                            // take the whole string
                            words.Add(summary);
                            summary = string.Empty;
                        }
                        else
                        {
                            words.Add(summary.Substring(0, nextSplit).Trim());
                            summary = summary.Substring(nextSplit);
                        }
                    }
                    else
                    {
                        words.Add(summary.Trim());
                        summary = "";
                    }
                }
                var rebuilt = string.Join(Environment.NewLine, words);
                return string.Format("{0}\n{1}{2}", issue.Key, rebuilt, timeSpent);
            }
        }


        private async Task<string> GetTotalTimeWorkedToday()
        {
            int ongoingMinutes = 0;
            if (currentRunningTracker != null)
            {
                ongoingMinutes = currentRunningTracker.Minutes;
            }

            var db = new Data.Database();
            var trackedToday = await db.Trackers(n => n.Start >= DateTime.Today);
            // include currently timed minutes
            var workspan = new TimeSpan(0, trackedToday.Sum(n => n.Minutes), 0)
                .Add(new TimeSpan(0, ongoingMinutes, 0));
            return string.Format("Worked {0}h {1}m today", workspan.Hours, workspan.Minutes);
        }

        /// <summary>
        /// Called when a tile resize animation completes. Removes tiles with no size, i.e. animation is complete.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ma_AnimationCompleted(object sender, EventArgs e)
        {
            foreach (Control control in TilePanel.Controls)
            {
                // remove this tile
                if (control.Size.IsEmpty)
                {
                    TilePanel.Controls.Remove(control);
                }
            }

        }

        /// <summary>
        /// Click on a tile to start/stop tracking time on it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tile_Click(object sender, EventArgs e)
        {
            var key = ((Control)sender).Name;
            TrackFromKey(key);
        }

        /// <summary>
        /// Window resizes rearranges the tiles.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TilePanel_Resize(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                ArrangeTiles(RecentTileFilterInput.Text);
            }
        }

        /// <summary>
        /// Entering filter text rearranges tiles.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecentTileFilterInput_TextChanged(object sender, EventArgs e)
        {
            ArrangeTiles(RecentTileFilterInput.Text);
        }

        /// <summary>
        /// Load time tracker history into the list.
        /// </summary>
        private async void LoadHistory()
        {
            var periodDays = GetHistoryPeriodDays();
            var startBefore = HistoryFilterDate.Value.Date.AddDays(1);
            var startAfter = HistoryFilterDate.Value.Date.AddDays(-periodDays);

            var db = new Data.Database();
            var col = await db.Trackers(n => n.Start >= startAfter && n.Start <= startBefore);

            // install custom groupings
            if (HistoryStartDateColumn.GroupKeyGetter == null)
            {
                // grouping key
                HistoryStartDateColumn.GroupKeyGetter = delegate (object rowObject)
                {
                    Model.Tracker tracker = (Model.Tracker)rowObject;

                    var today = DateTime.Today.Date;
                    var difference = (int)Math.Floor(DateTime.Today.Subtract(tracker.Start).Duration().TotalDays);

                    if (difference < 2)
                    {
                        return tracker.Start.Date;
                    }
                    else if (difference < 7)
                    {
                        return DateTime.Today.AddDays(-7);
                    }
                    else if (difference < 30)
                    {
                        return DateTime.Today.AddDays(-30);
                    }
                    else
                    {
                        return tracker.Start.Date;
                    }

                };
                // grouping title
                HistoryStartDateColumn.GroupKeyToTitleConverter = delegate (object rowObject)
                {
                    var dt = (DateTime)rowObject;
                    var difference = (int)Math.Floor(DateTime.Today.Subtract(dt).Duration().TotalDays);
                    if (difference < 1)
                        return "Today";
                    else if (difference < 2)
                        return "Yesterday";
                    else if (difference < 7)
                        return "This Week";
                    else if (difference < 30)
                        return "This Month";
                    else
                        return dt.ToString("ddd, dd MMM yyyy");
                };
            }

            var data = col.ToList();
            HistoryList.SetObjects(data);
            HistoryList.Sort(HistoryStartDateColumn, SortOrder.Descending);
        }

        /// <summary>
        /// Get the number of days from the period selection control.
        /// </summary>
        /// <returns></returns>
        private int GetHistoryPeriodDays()
        {
            const int defaultDays = 30;
            var parts = HistoryFilterPeriod.Text.Split(' ');
            if (parts.Length == 2)
            {
                var number = 0;

                if (int.TryParse(parts[0], out number))
                {
                    var multiplier = parts[1].ToLower();
                    if (multiplier.StartsWith("month"))
                        return number * 30;
                    else if (multiplier.StartsWith("week"))
                        return number * 7;
                    else if (multiplier.StartsWith("year"))
                        return number * 365;
                    else
                        return number;
                }
                else
                    return defaultDays;
            }
            else
            {
                return defaultDays;
            }
        }

        /// <summary>
        /// Refresh history when switching to the history tab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainTabpages_Selecting(object sender, TabControlCancelEventArgs e)
        {
            var page = (MetroFramework.Controls.MetroTabControl)sender;
            if (page.SelectedTab == HistoryTab)
            {
                LoadHistory();
            }
        }

        /// <summary>
        /// Refresh history on filter changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HistoryFilterPeriod_Validated(object sender, EventArgs e)
        {
            LoadHistory();
        }

        /// <summary>
        /// Refresh history on filter changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HistoryFilterDate_Validated(object sender, EventArgs e)
        {
            LoadHistory();
        }

        private void PushWorklogs_Click(object sender, EventArgs e)
        {
            SyncWorkLogs();
        }

        /// <summary>
        /// Send tracked times to jira as work logs.
        /// </summary>
        private async void SyncWorkLogs()
        {
            BusySpinner.Show();
            try
            {
                var ji = new Data.JiraIntegration(options);
                await ji.SyncWorklogs();
            }
            catch (AggregateException ex)
            {
                MessageBox.Show("There was a problem fetching your recent jira issues. Check your login details.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                BusySpinner.Hide();
                LoadHistory();
            }
        }

        private void HistoryApplyButton_Click(object sender, EventArgs e)
        {
            LoadHistory();
        }

        /// <summary>
        /// Switch between light/dark themes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DarkThemeSwitch_CheckedChanged(object sender, EventArgs e)
        {
            options.Dark = DarkThemeSwitch.Checked;
            metroStyleManager1.Theme = DarkThemeSwitch.Checked ? MetroFramework.MetroThemeStyle.Dark : MetroFramework.MetroThemeStyle.Light;

            // including the object listview
            if (options.Dark)
            {
                HistoryList.HeaderUsesThemes = false;
                HistoryList.HeaderFormatStyle = new BrightIdeasSoftware.HeaderFormatStyle()
                {
                    Normal = new BrightIdeasSoftware.HeaderStateStyle()
                    {
                        BackColor = Color.Black,
                        ForeColor = Color.White
                    },
                    Hot = new BrightIdeasSoftware.HeaderStateStyle()
                    {
                        BackColor = Color.White,
                        ForeColor = Color.Black
                    }
                };
                HistoryList.BackColor = Color.Black;
                HistoryList.ForeColor = Color.White;
                HistoryFilterPeriod.BackColor = Color.Black;
                HistoryFilterPeriod.ForeColor = Color.White;
                HistoryFilterDate.BackColor = Color.Black;
                HistoryFilterDate.ForeColor = Color.White;
            }
            else
            {
                HistoryList.HeaderUsesThemes = true;
                HistoryList.BackColor = SystemColors.Window;
                HistoryList.ForeColor = SystemColors.WindowText;

                HistoryFilterPeriod.BackColor = SystemColors.Window;
                HistoryFilterPeriod.ForeColor = SystemColors.WindowText;
                HistoryFilterDate.BackColor = SystemColors.Window;
                HistoryFilterDate.ForeColor = SystemColors.WindowText;
            }
        }

        /// <summary>
        /// Update display of the timer minutes rounding option.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RoundMinutesPicker_ValueChanged(object sender, EventArgs e)
        {
            options.RoundMinutesUp = RoundMinutesPicker.Value;
            if (options.RoundMinutesUp == 0)
                RoundMinutesLabel.Text = "No Rounding";
            else if (options.RoundMinutesUp == 1)
                RoundMinutesLabel.Text = "Round up by 1 minute";
            else
                RoundMinutesLabel.Text = string.Format("Round up to nearest {0} mins", options.RoundMinutesUp);
        }

        /// <summary>
        /// Apply the chosen form style.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StylePicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            options.StyleIndex = StylePicker.SelectedIndex - 1;
            metroStyleManager1.Style = (MetroFramework.MetroColorStyle)options.StyleIndex;
        }

        /// <summary>
        /// Save the jira login username on change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginNameInput_Validated(object sender, EventArgs e)
        {
            options.LoginName = LoginNameInput.Text;
        }

        /// <summary>
        /// Save the jira login secret on change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginSecretInput_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(LoginSecretInput.Text))
            {
                options.LoginSecret = Data.Security.EncryptString(LoginSecretInput.Text);
                LoginSecretInput.Clear();
                JiraIntegrationHintLabel.Show();
                SyncRecentIssues();
                SyncProjects();
            }
        }

        private void LoginSecretInput_Enter(object sender, EventArgs e)
        {
            JiraIntegrationHintLabel.Hide();
        }

        /// <summary>
        /// Save the jira login url on change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void JiraURLInput_Validated(object sender, EventArgs e)
        {
            options.JiraURL = JiraURLInput.Text;
        }

        /// <summary>
        /// Update the tracked minutes on the active timing issue.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void PeriodicTimer_Tick(object sender, EventArgs e)
        {
            // update the tile count on active trackers
            if (currentRunningTracker != null)
            {
                currentRunningTracker.Minutes = (int)Math.Round(DateTime.Now.Subtract(currentRunningTracker.Start).TotalMinutes);
                var tile = TilePanel.Controls.Find(currentRunningTracker.Key.ToString(), false).FirstOrDefault() as MetroFramework.Controls.MetroTile;
                if (tile != null)
                {
                    tile.TileCount = currentRunningTracker.Minutes;
                    tile.Refresh();
                }
            }

            TimeLoggedTodayLabel.Text = await GetTotalTimeWorkedToday();

        }


        /// <summary>
        /// Watch for issue keys on the clipboard.
        /// </summary>
        private async void DetectIssueKeysInClipboard()
        {
            if (options.AutoDetectFromClipboard)
            {
                var clip = Clipboard.GetText(TextDataFormat.UnicodeText);
                if (!string.IsNullOrEmpty(clip))
                {
                    // ignore clips already scanned
                    if (WatchClipboardTimer.Tag != null && clip == WatchClipboardTimer.Tag.ToString()) return;
                    WatchClipboardTimer.Tag = clip;

                    var db = new Data.Database();
                    var projectList = db.Projects(n => true).Result;
                    foreach (var project in projectList)
                    {
                        // build the regex to find the project key
                        var expression = string.Format(@"(\b{0}-\d+)", project.Key);
                        var match = System.Text.RegularExpressions.Regex.Match(clip, expression);
                        if (match.Success)
                        {
                            // notify to track if nothing is tracking, or the key is different than active tracker issue key
                            if (currentRunningTracker == null || currentRunningTracker.Key != match.Value)
                            {
                                // get issue summary
                                var issues = await db.Issues(n => n.Key == match.Value);
                                var issue = issues.FirstOrDefault();
                                var summary = (issue == null) ? "Track" : issue.Summary;
                                var title = string.Format("Time {0}", match.Value);
                                notifyIcon1.Tag = match.Value;
                                notifyIcon1.ShowBalloonTip(10000, title, summary, ToolTipIcon.Info);
                            }
                        }
                    }
                }
            }
        }

        private void AddIssueManuallyLink_Click(object sender, EventArgs e)
        {
            PromptForIssueKeyInput();
        }

        /// <summary>
        /// Update the active timer comment.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorklogComment_Validated(object sender, EventArgs e)
        {
            if (currentRunningTracker != null)
            {
                if (WorklogComment.Text != CommentPlaceholder)
                {
                    currentRunningTracker.Comment = WorklogComment.Text;
                }
            }
        }

        /// <summary>
        /// Update auto key detection option.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoDetectSwitch_CheckedChanged(object sender, EventArgs e)
        {
            options.AutoDetectFromClipboard = AutoDetectSwitch.Checked;
        }

        private void OpenInJIRAMenu_Click(object sender, EventArgs e)
        {
            try
            {
                var Tile = ((ContextMenuStrip)((ToolStripMenuItem)sender).Owner).SourceControl;
                if (Tile is MetroFramework.Controls.MetroTile)
                {
                    var issueKey = Tile.Name;
                    System.Diagnostics.Process.Start(string.Format("{0}/browse/{1}", options.JiraURL, issueKey));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void CopyJiraKeyMenu_Click(object sender, EventArgs e)
        {
            try
            {
                var Tile = ((ContextMenuStrip)((ToolStripMenuItem)sender).Owner).SourceControl;
                if (Tile is MetroFramework.Controls.MetroTile)
                {
                    SendToClipboard(Tile.Name);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void SendToClipboard(string issueKey)
        {
            Clipboard.SetText(issueKey, TextDataFormat.UnicodeText);
            // ignore the text for issue detection via clipboard
            notifyIcon1.Tag = issueKey;
            WatchClipboardTimer.Tag = issueKey;
            notifyIcon1.ShowBalloonTip(1000, "Copied", issueKey, ToolTipIcon.None);
        }

        /// <summary>
        /// Start / Stop tracking by issue key.
        /// </summary>
        /// <param name="issueKey"></param>
        private async void TrackFromKey(string issueKey)
        {
            // end current tracker if running, start a new tracker
            var tc = new TrackerController();
            currentRunningTracker = await tc.StartTracking(issueKey, options.RoundMinutesUp, currentRunningTracker);
            WorklogComment.Text = CommentPlaceholder;
            WorklogComment.ForeColor = SystemColors.GrayText;

            // sync worklogs
            SyncWorkLogs();

            // sync new issue keys
            var ji = new Data.JiraIntegration(options);
            await ji.SyncIssues();

            // Set the window title
            Text = Application.ProductName;
            if (currentRunningTracker != null)
            {
                // set the window title
                Text = string.Format("{0} - {1}", currentRunningTracker.Key, Application.ProductName);
            }

            // show/hide worklog comment
            if (currentRunningTracker == null)
            {
                if (WorklogAnimation != null) WorklogAnimation.Cancel();
                WorklogAnimation = new MetroFramework.Animation.MoveAnimation();
                WorklogAnimation.Start(WorklogCommentPanel, new Point(-WorklogCommentPanel.Width, WorklogCommentPanel.Top), MetroFramework.Animation.TransitionType.EaseInCubic, 30);
            }
            else
            {
                if (WorklogAnimation != null) WorklogAnimation.Cancel();
                WorklogAnimation = new MetroFramework.Animation.MoveAnimation();
                WorklogAnimation.Start(WorklogCommentPanel, new Point(0, WorklogCommentPanel.Top), MetroFramework.Animation.TransitionType.EaseInCubic, 30);
            }

            ArrangeTiles(RecentTileFilterInput.Text);
            TimeLoggedTodayLabel.Text = await GetTotalTimeWorkedToday();

        }

        private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            TrackFromKey(notifyIcon1.Tag.ToString());
        }

        private void WatchClipboardTimer_Tick(object sender, EventArgs e)
        {
            DetectIssueKeysInClipboard();
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                WindowState = FormWindowState.Minimized;
            }
        }

        /// <summary>
        /// Copy the current issue key to the clipboard on tray icon click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            if (currentRunningTracker != null)
            {
                SendToClipboard(currentRunningTracker.Key);
            }
        }

        /// <summary>
        /// Remove the worklog comment placeholder text on control enter.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorklogComment_Enter(object sender, EventArgs e)
        {
            if (WorklogComment.Text == CommentPlaceholder)
            {
                WorklogComment.Clear();
                WorklogComment.ForeColor = options.Dark ? SystemColors.ControlLight : SystemColors.ControlText;
            }
        }

        /// <summary>
        /// Restore the worklog comment placeholder text on control leave.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorklogComment_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(WorklogComment.Text))
            {
                WorklogComment.Text = CommentPlaceholder;
                WorklogComment.ForeColor = SystemColors.GrayText;
            }
        }
    }
}
