﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ti.Me.Model
{
    public class Issue
    {
        [BsonId]
        public int IssueId { get; set; }
        public string Key { get; set; }
        public string Summary { get; set; }
        public int Minutes { get; set; }
        public string TimeSpent { get; set; }
        public DateTime? LastTracked { get; set; }
        public bool Synced { get; set; }
        public bool Valid { get; set; }
    }
}
