﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ti.Me.Model
{
    internal class Tracker
    {
        [BsonId]
        public int TrackerId { get; set; }
        public string Key { get; set; }
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        public int Minutes { get; set; }
        public bool SyncedWithJira { get; set; }
        public string Comment { get; set; }
    }
}
