﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ti.Me.Model
{
    public class Options
    {
        public int Id { get; set; }
        public int Version { get; set; }
        public bool Dark { get; set; }
        public int RoundMinutesUp { get; set; }
        public int StyleIndex { get; set; }
        public string JiraURL { get; set; }
        public string LoginName { get; set; }
        public string LoginSecret { get; set; }
        public bool AutoDetectFromClipboard { get; set; }
    }
}
