﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ti.Me.Model
{
    internal class Worklog
    {
        [BsonId]
        public int WorklogId { get; set; }
        public string Key { get; set; }
        public string Author { get; set; }
        public string Comment { get; set; }
        public DateTime? CreateDate { get; set; }
        public int TimeSpentInSeconds { get; set; }
        public string TimeSpent { get; set; }
    }
}
