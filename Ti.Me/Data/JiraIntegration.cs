﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;

namespace Ti.Me.Data
{
    public class JiraIntegration
    {
        private bool syncEnabled = false;
        private Model.Options options;

        public JiraIntegration(Model.Options options)
        {
            this.options = options;
        }

        private Jira Init()
        {
            if (string.IsNullOrEmpty(options.JiraURL) || string.IsNullOrEmpty(options.LoginName) || string.IsNullOrEmpty(options.LoginSecret))
            {
                syncEnabled = false;
                return null;
            }
            else
            {
                var jira = Jira.CreateRestClient(options.JiraURL, options.LoginName, Data.Security.DecryptString(options.LoginSecret));
                syncEnabled = true;
                return jira;
            }
        }

        public Task SyncIssues()
        {
            return Task.Run(() =>
            {
                var jira = Init();
                if (!syncEnabled) return;

                var db = new Data.Database();

                // get missing issue details
                var col = db.Issues(n => n.Synced == false).Result;

                foreach (var issue in col)
                {
                    try
                    {
                        var jiraDetails = jira.Issues.GetIssueAsync(issue.Key).Result;
                        if (jiraDetails != null)
                        {
                            issue.Key = jiraDetails.Key.Value;
                            issue.Summary = jiraDetails.Summary;
                            issue.Valid = true;
                            issue.Synced = true;
                            if (jiraDetails.TimeTrackingData.TimeSpentInSeconds != null)
                            {
                                issue.Minutes = jiraDetails.TimeTrackingData.TimeSpentInSeconds.Value / 60;
                                issue.TimeSpent = jiraDetails.TimeTrackingData.TimeSpent;
                            }
                            var n = db.UpsertIssue(issue).Result;
                        }
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        issue.Synced = true;
                        issue.Valid = false;
                        var n = db.UpsertIssue(issue).Result;
                    }
                }
            });
        }

        public Task SyncRecentIssues(string username)
        {
            var jira = Init();
            if (!syncEnabled) return Task.CompletedTask;

            return Task.Run(() =>
            {
                var db = new Data.Database();

                // get recent issues
                var issues = (from i in jira.Issues.Queryable
                              where i.Assignee == username
                              orderby i.Updated descending
                              select i).Take(30).ToList();

                foreach (var jiraIssue in issues)
                {
                    var issueSearch = db.Issues(n => n.Key == jiraIssue.Key.Value);
                    var issue = issueSearch.Result.FirstOrDefault();

                    if (issue == null)
                    {
                        issue = new Model.Issue();
                    }

                    issue.Key = jiraIssue.Key.Value;
                    issue.Summary = jiraIssue.Summary;
                    issue.Synced = true;
                    issue.Valid = true;

                    if (jiraIssue.TimeTrackingData.TimeSpentInSeconds != null)
                    {
                        issue.Minutes = jiraIssue.TimeTrackingData.TimeSpentInSeconds.Value / 60;
                        issue.TimeSpent = jiraIssue.TimeTrackingData.TimeSpent;
                    }

                    // add all unsynced times
                    var unsyncedTrackers = db.Trackers(n => n.Key == issue.Key && n.SyncedWithJira == false).Result;
                    issue.Minutes += unsyncedTrackers.Sum(n => n.Minutes);
                    db.UpsertIssue(issue);
                }
            });

        }

        public Task<bool> SyncWorklogs()
        {
            return Task.Run(() =>
            {
                var jira = Init();
                if (!syncEnabled) return false;

                // sync any unsynced issues
                SyncIssues();

                // fetch unsynced trackers
                var db = new Data.Database();
                var unsyncedTrackers = db.Trackers(n => n.SyncedWithJira == false).Result;

                foreach (var tracker in unsyncedTrackers)
                {
                    var issue = db.GetIssue(tracker.Key).Result;
                    if (issue != null && issue.Valid)
                    {
                        var wl = new Worklog(string.Format("{0} m", tracker.Minutes), tracker.Start, tracker.Comment);
                        jira.Issues.AddWorklogAsync(tracker.Key, wl);
                        tracker.SyncedWithJira = true;
                        db.UpsertTracker(tracker);
                    }
                }
                return true;
            });
        }

        public Task<Model.Issue> Search(string key)
        {
            return Task.Run<Model.Issue>(() =>
            {
                var jira = Init();
                if (!syncEnabled) return null;
                try
                {
                    var match = jira.Issues.GetIssueAsync(key).Result;
                    var issue = new Model.Issue()
                    {
                        Key = match.Key.Value,
                        Summary = match.Summary,
                        TimeSpent = match.TimeTrackingData.TimeSpent,
                        Valid = true,
                        Synced = true
                    };

                    if (match.TimeTrackingData != null && match.TimeTrackingData.TimeSpentInSeconds != null)
                    {
                        issue.Minutes = match.TimeTrackingData.TimeSpentInSeconds.Value / 60;
                    }

                    return issue;
                }
                catch (Exception)
                {
                    return null;
                }
            });
        }

        public Task SyncProjects()
        {
            return Task.Run(() =>
            {
                var jira = Init();
                if (!syncEnabled) return;
                var db = new Data.Database();
                var jiraProjects = jira.Projects.GetProjectsAsync().Result;
                foreach (var p in jiraProjects)
                {
                    var localproject = db.Projects(n => n.Key == p.Key).Result.FirstOrDefault();
                    if (localproject == null)
                    {
                        db.UpsertProject(new Model.Project()
                        {
                            Key = p.Key,
                            Name = p.Name
                        });
                    }
                }
            });
        }

    }
}
