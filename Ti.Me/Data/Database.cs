﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Linq.Expressions;
using LiteDB;
using Ti.Me.Model;

namespace Ti.Me.Data
{
    internal class Database
    {

        private LiteDatabase db;

        public Database()
        {
            var path = GetDatabasePath();
            db = new LiteDatabase(path);
            MigrateDatabase();
        }

        public string GetDatabasePath()
        {
            const string datafilename = "time.db";
            string directorypath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
            string fullpath = System.IO.Path.Combine(directorypath, datafilename);
            System.IO.Directory.CreateDirectory(directorypath);
            return fullpath;
        }

        private void MigrateDatabase()
        {
            var col = db.GetCollection<Options>();
            var versionRow = col.FindOne(n => true);
            if (versionRow == null)
            {
                versionRow = new Options() { Version = 1, StyleIndex = -1 };
                col.Insert(versionRow);
            }

            db.GetCollection<Model.Issue>().EnsureIndex(n => n.Key, true);
            db.GetCollection<Model.Issue>().EnsureIndex(n => n.Summary);
            db.GetCollection<Model.Worklog>().EnsureIndex(n => n.Key);
            db.GetCollection<Model.Tracker>().EnsureIndex(n => n.Key);
            db.GetCollection<Model.Project>().EnsureIndex(n => n.Key, true);
        }

        public Task<IEnumerable<Model.Tracker>> Trackers(Expression<Func<Tracker, bool>> predicate)
        {
            return Task.Run(() =>
            {
                var col = db.GetCollection<Tracker>();
                return col.Find(predicate);
                //return col.FindAll().AsQueryable().Where(predicate);
            });
        }

        public Task<IEnumerable<Model.Tracker>> Trackers()
        {
            return Trackers(n => true);
        }

        public Task<IEnumerable<Model.Issue>> Issues(Expression<Func<Issue, bool>> predicate)
        {
            return Task.Run(() =>
            {
                var col = db.GetCollection<Issue>();
                return col.Find(predicate);
            });
        }

        public Task<IEnumerable<Model.Issue>> Issues()
        {
            return Issues(n => true);
        }

        public Task<IEnumerable<Model.Worklog>> Worklogs(Expression<Func<Worklog, bool>> predicate)
        {
            return Task.Run(() =>
            {
                var col = db.GetCollection<Worklog>();
                return col.FindAll().AsQueryable().Where(predicate).AsEnumerable();
            });
        }

        public Task<Tracker> UpsertTracker(Tracker tracker)
        {
            return Task.Run(() =>
            {
                var col = db.GetCollection<Model.Tracker>();
                col.Upsert(tracker);
                return tracker;
            });
        }

        public Task<Issue> UpsertIssue(Issue issue)
        {
            return Task.Run(() =>
            {
                var col = db.GetCollection<Model.Issue>();
                col.Upsert(issue);
                return issue;
            });
        }

        public Options GetOptions()
        {
            var col = db.GetCollection<Options>().FindAll();
            if (col.Any())
            {
                return col.First();
            }
            else
            {
                return new Options()
                {
                    StyleIndex = -1
                };
            }
        }

        public void UpsertOptions(Options options)
        {
            var col = db.GetCollection<Options>();
            col.Upsert(options);
        }

        public Task<IEnumerable<Model.Project>> Projects(Expression<Func<Project, bool>> predicate)
        {
            return Task.Run(() =>
            {
                return db.GetCollection<Project>().Find(predicate);
            });
        }

        public void UpsertProject(Project project)
        {
            var col = db.GetCollection<Project>();
            col.Upsert(project);
        }

        public Task<Issue> GetIssue(string key)
        {
            return Task.Run(() =>
            {
                key = key.ToUpper();
                var db = new Data.Database();
                var issue = db.Issues(n => n.Key.ToUpper() == key).Result;
                if (issue.Any())
                {
                    return issue.First();
                }
                else
                {
                    var newIssue = new Issue()
                    {
                        Key = key,
                        Summary = string.Empty
                    };
                    return db.UpsertIssue(newIssue).Result;
                }

            });
        }

    }
}
